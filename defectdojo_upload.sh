for i; do
   echo $i
done
curl -X POST "$1/import-scan/" -H "Authorization: Token $2" -H "accept: application/json" -H  "Content-Type: multipart/form-data"  -F "scan_date=$3" -F "minimum_severity=Info" -F "active=true" -F "verified=true" -F "scan_type=$6" -F "file=@$5;type=application/json" -F "engagement=$4" -F "close_old_findings=true" -F "push_to_jira=false"